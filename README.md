# Handyman

Handyman is a command line tool that can help scaffold files in any code project

## Installation

Use [npm](https://www.npmjs.com/package/handymanjs) to install Handyman.

```bash
npm i -g handymanjs
```

## Usage

### `handyman generate`

```bash
-c --config <string> Configuration path Ex: ./handyman.config.json
-p --pattern <string> Pattern string to replace in the template files Ex: __component__
-r --replace <string> String to replace pattern Ex: SubmitButton will replace __component__ within the template files
-t --template <string> Name of the template used to generate the files
-tp --templatesPath <string> Path for templates
-y --overwrite Skip overwrite prompt
```

**WARNING**
If you decide to use the --templatesPath option, it will override the path within your config

Ex:

```bash
handyman generate ./__output__/ -r Page -t page -p __page__
handyman generate ./__output__/ -r ["Page","App"] -t multi -p ["__page__","__app__"]
```

### Configuration

A configuration file in the format of JSON should be provided to provide a more refined experience. An example of one would be like the following

```json
{
  "templateFilesPath": "some/path/to/my_templates"
}
```

An example of what the folder of templates could look like is the following

```
__templates__
├── component
│   ├── __component__.module.scss
│   ├── __component__.test.tsx
│   ├── __component__.tsx
│   └── index.js
└── page
    ├── index.js
    └── __page__.tsx
```

And a file could look like the following

```
import styles from "./__component__.module.scss";

interface __component__Props {}

export default function __component__(props: __component__Props) {
  return (
    <div
      className={styles["__component__(kebabCase)"]}
      data-testid="__component__"
    >
      <h1>__component__(titleCase)</h1>
    </div>
  );
}
```

If you want to change the casing to any of the replaced string you can add parenthesis and the typing case to change it. If none are added it will just default to Pascal Case.

```
(noCase)       // Lives down BY the River
(camelCase)    // livesDownByTheRiver
(constantCase) // LIVES_DOWN_BY_THE_RIVER
(dotCase)      // lives.down.by.the.river
(kebabCase)    // lives-down-by-the-river
(lowerCase)    // livesdownbytheriver
(pascalCase)   // LivesDownByTheRiver
(pathCase)     // lives/down/by/the/river
(sentenceCase) // Lives down by the river
(snakeCase)    // lives_down_by_the_river
(titleCase)    // Lives Down By The River
```

## Todo

1. Remove the need for the package to be installed globally.
1. Add an interactive mode that would be not require any of the options to be put Ex: `handyman interactive`
   1. For this to work we would need a better configuration that would have all the options predefined

## Contributing

Pull requests are welcome. For major changes please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
