const { camelCase, lowerCase, snakeCase, startCase, upperCase, upperFirst } = require('lodash');

class StringHelper {

    static toArrayFromArrayStr(str) {
        return str.replace(/\[|\]/g, '').split(',');
    }

    static toCamelCase(str) {
        return camelCase(str);
    }

    static toTitleCase(str) {
        return startCase(camelCase(str));
    }

    static toPascalCase(str) {
        return startCase(camelCase(str)).replace(/ /g, '');
    }

    static toConstantCase(str) {
        return upperCase(str).replace(/ /g, '_');
    }

    static toDotCase(str) {
        return lowerCase(str).replace(/ /g, '.');
    }

    static toKebabCase(str) {
        return str.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
    }

    static toLowerCase(str) {
        return lowerCase(str).replace(/ /g, '');
    }

    static toPathCase(str) {
        return lowerCase(str).replace(/ /g, '/');
    }

    static toSnakeCase(str) {
        return snakeCase(str);
    }

    static toSentenceCase(str) {
        return upperFirst(lowerCase(str));
    }

    static fileUpdater(fileStr, templateToReplace, strToReplace) {
        const caseRegex = new RegExp(/\(\w+Case\)/, 'g');
        const parenthesisRegex = new RegExp(/\(|\)/, 'g');

        const splitStr = fileStr.split(templateToReplace);
        const finalStr = splitStr.map((str, idx) => {
            if (idx === 0) {
                return str;
            }
            const casingMatch = [...str.matchAll(caseRegex)][0];
            if (casingMatch) {
                const casingType = `to${this.toPascalCase(casingMatch[0].replace(parenthesisRegex, ''))}`;
                return `${this[casingType](strToReplace)}${str.replace(casingMatch[0], '')}`
            }
            return `${this.toPascalCase(strToReplace)}${str}`
        })
        return finalStr.join('')
    }
}

module.exports = StringHelper;