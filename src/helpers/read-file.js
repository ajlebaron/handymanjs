const fs = require('fs')
const path = require('path');
const StringHelper = require('./StringHelper');

function readFiles(templatesPath, type, templateToReplace, strToReplace) {
    if (templateToReplace.length !== strToReplace.length) {
        throw new Error('Replacement array must be the same length as the Pattern array');
    }

    const filePath = path.resolve(process.cwd(), `${templatesPath}/${type}`);
    const filesNames = fs.readdirSync(filePath);
    const updatedNames = filesNames.map(name => {
        let updatedName = name;
        templateToReplace.forEach((template, idx) => {
            updatedName = StringHelper.fileUpdater(updatedName, template, strToReplace[idx])
        })
        return updatedName;
    });
    const fileContents = filesNames.map(content => {
        let updatedContent = fs.readFileSync(path.resolve(process.cwd(), `${filePath}/${content}`), 'utf8');
        templateToReplace.forEach((template, idx) => {
            updatedContent = StringHelper.fileUpdater(updatedContent, template, strToReplace[idx])
        })
        return updatedContent
    })

    return { fileContents, fileNames: updatedNames }
}

module.exports = readFiles;