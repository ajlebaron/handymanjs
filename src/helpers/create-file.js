const fs = require('fs');
const path = require('path');
const StringHelper = require('./StringHelper');
const prompt = require('prompt-sync')({ sigint: true });


function createFiles(inputPath, files, directoryName, skipPrompt) {
    let filesAlreadyExistInPath = false;
    const { fileContents, fileNames } = files;
    const folderPath = path.join(process.cwd(), `${inputPath}/${StringHelper.toPascalCase(directoryName)}`)
    if (!fs.existsSync(folderPath)) {
        fs.mkdirSync(folderPath);
    }

    if (!skipPrompt) {
        fileNames.forEach((fileName) => {
            const filePath = path.join(folderPath, `/${fileName}`)
            if (fs.existsSync(filePath)) {
                filesAlreadyExistInPath = true;
            }
        })
    }

    if (filesAlreadyExistInPath) {
        const response = prompt('Files already exist in that path, overwrite? [Y/n]: ');
        if (response === 'Y') {
            fileNames.forEach((fileName, idx) => {
                const content = fileContents[idx];
                const filePath = path.join(folderPath, `/${fileName}`)

                fs.writeFile(filePath, content, err => {
                    if (err) throw err;
                    console.log("Created file: ", filePath);
                    return true;
                });
            })
        } else {
            console.log("Aborting operation")
        }
    } else {
        fileNames.forEach((fileName, idx) => {
            const content = fileContents[idx];
            const filePath = path.join(folderPath, `/${fileName}`)

            fs.writeFile(filePath, content, err => {
                if (err) throw err;
                console.log("Created file: ", filePath);
                return true;
            });
        })
    }
}

module.exports = createFiles;