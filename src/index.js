#! /usr/bin/env node
const { Command } = require('commander');
const fs = require('fs');
const path = require('path');
const createFiles = require('./helpers/create-file');
const readFiles = require('./helpers/read-file');
const StringHelper = require('./helpers/StringHelper');

const program = new Command();

program
    .name('handyman')
    .description('CLI command to scaffold files')
    .version('0.3.5');

program.command('generate')
    .description('Generate files based on input')
    .argument('<string>', 'Path to where files will be generated')
    .option('-c, --config <string>', 'Config path', './handyman.config.json')
    .option('-p, --pattern <string>', 'Pattern string to replace', '__component__')
    .option('-r, --replace <string>', 'Name of string to replace')
    .option('-t, --template <string>', 'Type of template used')
    .option('-tp, --templatesPath <string>', 'Path for templates')
    .option('-y, --overwrite', 'Skip overwrite prompt')
    .action((str, options) => {
        const { overwrite, pattern, replace, template } = options;
        console.log(options);
        try {
            if (!replace) {
                throw new Error("Please specify a string to replace the pattern string via -r or --replace");
            } else if (!template) {
                throw new Error("Please specify a template to replace via -t or --template");
            } else if (!str) {
                throw new Error("Add a path to where you want to place the files");
            }

            let config;
            if (options.config) {
                const configUrl = path.resolve(process.cwd(), options.config);
                config = JSON.parse(fs.readFileSync(configUrl).toString());
            }
            if (!options.templatesPath && !config.templateFilesPath) {
                throw new Error("Add a path to where your templates are located");
            }

            const templatesPath = path.resolve(process.cwd(), options.templatesPath ?? config.templateFilesPath);
            const files = readFiles(templatesPath, template, StringHelper.toArrayFromArrayStr(pattern), StringHelper.toArrayFromArrayStr(replace));
            createFiles(str, files, replace, !!overwrite)
        } catch (error) {
            console.log(error);
        }
    });

program.parse();